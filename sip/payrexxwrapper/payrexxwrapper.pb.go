// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.18.1
// source: sip/payrexxwrapper/payrexxwrapper.proto

package payrexxwrapper

import (
	money "gitlab.ethz.ch/vseth/0403-isg/libraries/protostub-golang/vseth/type/money"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// State specifies the health of a gateway.
type Gateway_State int32

const (
	Gateway_STATE_UNSPECIFIED Gateway_State = 0
	Gateway_OK                Gateway_State = 1
	Gateway_WARNING           Gateway_State = 2
	Gateway_ERROR             Gateway_State = 3
)

// Enum value maps for Gateway_State.
var (
	Gateway_State_name = map[int32]string{
		0: "STATE_UNSPECIFIED",
		1: "OK",
		2: "WARNING",
		3: "ERROR",
	}
	Gateway_State_value = map[string]int32{
		"STATE_UNSPECIFIED": 0,
		"OK":                1,
		"WARNING":           2,
		"ERROR":             3,
	}
)

func (x Gateway_State) Enum() *Gateway_State {
	p := new(Gateway_State)
	*p = x
	return p
}

func (x Gateway_State) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (Gateway_State) Descriptor() protoreflect.EnumDescriptor {
	return file_sip_payrexxwrapper_payrexxwrapper_proto_enumTypes[0].Descriptor()
}

func (Gateway_State) Type() protoreflect.EnumType {
	return &file_sip_payrexxwrapper_payrexxwrapper_proto_enumTypes[0]
}

func (x Gateway_State) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use Gateway_State.Descriptor instead.
func (Gateway_State) EnumDescriptor() ([]byte, []int) {
	return file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescGZIP(), []int{0, 0}
}

// Status specifies where in the payment flow this gateway is at this time.
type Gateway_Status int32

const (
	Gateway_STATUS_UNSPECIFIED Gateway_Status = 0
	Gateway_UNKNOWN            Gateway_Status = 1
	Gateway_OPEN               Gateway_Status = 2
	Gateway_CONFIRMED          Gateway_Status = 3
	Gateway_COMPLETED          Gateway_Status = 4
	Gateway_EXPIRED            Gateway_Status = 5
)

// Enum value maps for Gateway_Status.
var (
	Gateway_Status_name = map[int32]string{
		0: "STATUS_UNSPECIFIED",
		1: "UNKNOWN",
		2: "OPEN",
		3: "CONFIRMED",
		4: "COMPLETED",
		5: "EXPIRED",
	}
	Gateway_Status_value = map[string]int32{
		"STATUS_UNSPECIFIED": 0,
		"UNKNOWN":            1,
		"OPEN":               2,
		"CONFIRMED":          3,
		"COMPLETED":          4,
		"EXPIRED":            5,
	}
)

func (x Gateway_Status) Enum() *Gateway_Status {
	p := new(Gateway_Status)
	*p = x
	return p
}

func (x Gateway_Status) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (Gateway_Status) Descriptor() protoreflect.EnumDescriptor {
	return file_sip_payrexxwrapper_payrexxwrapper_proto_enumTypes[1].Descriptor()
}

func (Gateway_Status) Type() protoreflect.EnumType {
	return &file_sip_payrexxwrapper_payrexxwrapper_proto_enumTypes[1]
}

func (x Gateway_Status) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use Gateway_Status.Descriptor instead.
func (Gateway_Status) EnumDescriptor() ([]byte, []int) {
	return file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescGZIP(), []int{0, 1}
}

type Gateway struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Required except for creation.
	Name string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	// Input only
	// Amount to charge for. Only required for creation.
	Amount *money.Money `protobuf:"bytes,2,opt,name=amount,proto3" json:"amount,omitempty"`
	// Input only
	// Location to redirect the user to after the payment process. Only required when creating a new gateway.
	RedirectUrl string `protobuf:"bytes,3,opt,name=redirect_url,json=redirectUrl,proto3" json:"redirect_url,omitempty"`
	// Timestamp when the gateway expires. In requests, this value is the latest timestamp where the gateway should expire. The response may have a different value.
	ExpireTime *timestamppb.Timestamp `protobuf:"bytes,4,opt,name=expire_time,json=expireTime,proto3" json:"expire_time,omitempty"`
	// Output only
	// Payrexx URL where the user can proceed with the payment.
	PaymentUrl string `protobuf:"bytes,5,opt,name=payment_url,json=paymentUrl,proto3" json:"payment_url,omitempty"`
	// Fee charged for the transaction.
	Fee *money.Money `protobuf:"bytes,6,opt,name=fee,proto3" json:"fee,omitempty"`
	// Output only
	// State of the gateway representing the health of the gateway.
	State Gateway_State `protobuf:"varint,7,opt,name=state,proto3,enum=sip.payrexxwrapper.Gateway_State" json:"state,omitempty"`
	// Output only
	// Status of the gateway representing where in the flow the gateway is.
	Status Gateway_Status `protobuf:"varint,8,opt,name=status,proto3,enum=sip.payrexxwrapper.Gateway_Status" json:"status,omitempty"`
	// Input only
	// Reference name for identification of the gateway within pay. Only required for creation.
	ReferenceName string `protobuf:"bytes,9,opt,name=reference_name,json=referenceName,proto3" json:"reference_name,omitempty"`
}

func (x *Gateway) Reset() {
	*x = Gateway{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Gateway) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Gateway) ProtoMessage() {}

func (x *Gateway) ProtoReflect() protoreflect.Message {
	mi := &file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Gateway.ProtoReflect.Descriptor instead.
func (*Gateway) Descriptor() ([]byte, []int) {
	return file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescGZIP(), []int{0}
}

func (x *Gateway) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *Gateway) GetAmount() *money.Money {
	if x != nil {
		return x.Amount
	}
	return nil
}

func (x *Gateway) GetRedirectUrl() string {
	if x != nil {
		return x.RedirectUrl
	}
	return ""
}

func (x *Gateway) GetExpireTime() *timestamppb.Timestamp {
	if x != nil {
		return x.ExpireTime
	}
	return nil
}

func (x *Gateway) GetPaymentUrl() string {
	if x != nil {
		return x.PaymentUrl
	}
	return ""
}

func (x *Gateway) GetFee() *money.Money {
	if x != nil {
		return x.Fee
	}
	return nil
}

func (x *Gateway) GetState() Gateway_State {
	if x != nil {
		return x.State
	}
	return Gateway_STATE_UNSPECIFIED
}

func (x *Gateway) GetStatus() Gateway_Status {
	if x != nil {
		return x.Status
	}
	return Gateway_STATUS_UNSPECIFIED
}

func (x *Gateway) GetReferenceName() string {
	if x != nil {
		return x.ReferenceName
	}
	return ""
}

type GetGatewayRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Required
	Name string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
}

func (x *GetGatewayRequest) Reset() {
	*x = GetGatewayRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetGatewayRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetGatewayRequest) ProtoMessage() {}

func (x *GetGatewayRequest) ProtoReflect() protoreflect.Message {
	mi := &file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetGatewayRequest.ProtoReflect.Descriptor instead.
func (*GetGatewayRequest) Descriptor() ([]byte, []int) {
	return file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescGZIP(), []int{1}
}

func (x *GetGatewayRequest) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

type BatchRefreshGatewaysRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Required
	Names []string `protobuf:"bytes,1,rep,name=names,proto3" json:"names,omitempty"`
}

func (x *BatchRefreshGatewaysRequest) Reset() {
	*x = BatchRefreshGatewaysRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BatchRefreshGatewaysRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BatchRefreshGatewaysRequest) ProtoMessage() {}

func (x *BatchRefreshGatewaysRequest) ProtoReflect() protoreflect.Message {
	mi := &file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BatchRefreshGatewaysRequest.ProtoReflect.Descriptor instead.
func (*BatchRefreshGatewaysRequest) Descriptor() ([]byte, []int) {
	return file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescGZIP(), []int{2}
}

func (x *BatchRefreshGatewaysRequest) GetNames() []string {
	if x != nil {
		return x.Names
	}
	return nil
}

type BatchRefreshGatewaysResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Required
	Gateways []*Gateway `protobuf:"bytes,1,rep,name=gateways,proto3" json:"gateways,omitempty"`
}

func (x *BatchRefreshGatewaysResponse) Reset() {
	*x = BatchRefreshGatewaysResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BatchRefreshGatewaysResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BatchRefreshGatewaysResponse) ProtoMessage() {}

func (x *BatchRefreshGatewaysResponse) ProtoReflect() protoreflect.Message {
	mi := &file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BatchRefreshGatewaysResponse.ProtoReflect.Descriptor instead.
func (*BatchRefreshGatewaysResponse) Descriptor() ([]byte, []int) {
	return file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescGZIP(), []int{3}
}

func (x *BatchRefreshGatewaysResponse) GetGateways() []*Gateway {
	if x != nil {
		return x.Gateways
	}
	return nil
}

type BatchUpdateGatewayFeesRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Required
	// Gateways populated with the charged fee from Payrexx.
	Gateways []*Gateway `protobuf:"bytes,1,rep,name=gateways,proto3" json:"gateways,omitempty"`
}

func (x *BatchUpdateGatewayFeesRequest) Reset() {
	*x = BatchUpdateGatewayFeesRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BatchUpdateGatewayFeesRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BatchUpdateGatewayFeesRequest) ProtoMessage() {}

func (x *BatchUpdateGatewayFeesRequest) ProtoReflect() protoreflect.Message {
	mi := &file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BatchUpdateGatewayFeesRequest.ProtoReflect.Descriptor instead.
func (*BatchUpdateGatewayFeesRequest) Descriptor() ([]byte, []int) {
	return file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescGZIP(), []int{4}
}

func (x *BatchUpdateGatewayFeesRequest) GetGateways() []*Gateway {
	if x != nil {
		return x.Gateways
	}
	return nil
}

type BatchUpdateGatewayFeesResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Output only
	// Gateways which are not initiated by payment-api
	WarningNames []string `protobuf:"bytes,1,rep,name=warning_names,json=warningNames,proto3" json:"warning_names,omitempty"`
}

func (x *BatchUpdateGatewayFeesResponse) Reset() {
	*x = BatchUpdateGatewayFeesResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BatchUpdateGatewayFeesResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BatchUpdateGatewayFeesResponse) ProtoMessage() {}

func (x *BatchUpdateGatewayFeesResponse) ProtoReflect() protoreflect.Message {
	mi := &file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BatchUpdateGatewayFeesResponse.ProtoReflect.Descriptor instead.
func (*BatchUpdateGatewayFeesResponse) Descriptor() ([]byte, []int) {
	return file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescGZIP(), []int{5}
}

func (x *BatchUpdateGatewayFeesResponse) GetWarningNames() []string {
	if x != nil {
		return x.WarningNames
	}
	return nil
}

var File_sip_payrexxwrapper_payrexxwrapper_proto protoreflect.FileDescriptor

var file_sip_payrexxwrapper_payrexxwrapper_proto_rawDesc = []byte{
	0x0a, 0x27, 0x73, 0x69, 0x70, 0x2f, 0x70, 0x61, 0x79, 0x72, 0x65, 0x78, 0x78, 0x77, 0x72, 0x61,
	0x70, 0x70, 0x65, 0x72, 0x2f, 0x70, 0x61, 0x79, 0x72, 0x65, 0x78, 0x78, 0x77, 0x72, 0x61, 0x70,
	0x70, 0x65, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x12, 0x73, 0x69, 0x70, 0x2e, 0x70,
	0x61, 0x79, 0x72, 0x65, 0x78, 0x78, 0x77, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72, 0x1a, 0x1b, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65,
	0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1f, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x74, 0x69, 0x6d, 0x65,
	0x73, 0x74, 0x61, 0x6d, 0x70, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x16, 0x76, 0x73, 0x65,
	0x74, 0x68, 0x2f, 0x74, 0x79, 0x70, 0x65, 0x2f, 0x6d, 0x6f, 0x6e, 0x65, 0x79, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x22, 0xae, 0x04, 0x0a, 0x07, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x12,
	0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e,
	0x61, 0x6d, 0x65, 0x12, 0x29, 0x0a, 0x06, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x11, 0x2e, 0x76, 0x73, 0x65, 0x74, 0x68, 0x2e, 0x74, 0x79, 0x70, 0x65,
	0x2e, 0x4d, 0x6f, 0x6e, 0x65, 0x79, 0x52, 0x06, 0x61, 0x6d, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x21,
	0x0a, 0x0c, 0x72, 0x65, 0x64, 0x69, 0x72, 0x65, 0x63, 0x74, 0x5f, 0x75, 0x72, 0x6c, 0x18, 0x03,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x72, 0x65, 0x64, 0x69, 0x72, 0x65, 0x63, 0x74, 0x55, 0x72,
	0x6c, 0x12, 0x3b, 0x0a, 0x0b, 0x65, 0x78, 0x70, 0x69, 0x72, 0x65, 0x5f, 0x74, 0x69, 0x6d, 0x65,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61,
	0x6d, 0x70, 0x52, 0x0a, 0x65, 0x78, 0x70, 0x69, 0x72, 0x65, 0x54, 0x69, 0x6d, 0x65, 0x12, 0x1f,
	0x0a, 0x0b, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x5f, 0x75, 0x72, 0x6c, 0x18, 0x05, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x0a, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x6e, 0x74, 0x55, 0x72, 0x6c, 0x12,
	0x23, 0x0a, 0x03, 0x66, 0x65, 0x65, 0x18, 0x06, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x11, 0x2e, 0x76,
	0x73, 0x65, 0x74, 0x68, 0x2e, 0x74, 0x79, 0x70, 0x65, 0x2e, 0x4d, 0x6f, 0x6e, 0x65, 0x79, 0x52,
	0x03, 0x66, 0x65, 0x65, 0x12, 0x37, 0x0a, 0x05, 0x73, 0x74, 0x61, 0x74, 0x65, 0x18, 0x07, 0x20,
	0x01, 0x28, 0x0e, 0x32, 0x21, 0x2e, 0x73, 0x69, 0x70, 0x2e, 0x70, 0x61, 0x79, 0x72, 0x65, 0x78,
	0x78, 0x77, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72, 0x2e, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79,
	0x2e, 0x53, 0x74, 0x61, 0x74, 0x65, 0x52, 0x05, 0x73, 0x74, 0x61, 0x74, 0x65, 0x12, 0x3a, 0x0a,
	0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x08, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x22, 0x2e,
	0x73, 0x69, 0x70, 0x2e, 0x70, 0x61, 0x79, 0x72, 0x65, 0x78, 0x78, 0x77, 0x72, 0x61, 0x70, 0x70,
	0x65, 0x72, 0x2e, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75,
	0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x25, 0x0a, 0x0e, 0x72, 0x65, 0x66,
	0x65, 0x72, 0x65, 0x6e, 0x63, 0x65, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x09, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0d, 0x72, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65, 0x4e, 0x61, 0x6d, 0x65,
	0x22, 0x3e, 0x0a, 0x05, 0x53, 0x74, 0x61, 0x74, 0x65, 0x12, 0x15, 0x0a, 0x11, 0x53, 0x54, 0x41,
	0x54, 0x45, 0x5f, 0x55, 0x4e, 0x53, 0x50, 0x45, 0x43, 0x49, 0x46, 0x49, 0x45, 0x44, 0x10, 0x00,
	0x12, 0x06, 0x0a, 0x02, 0x4f, 0x4b, 0x10, 0x01, 0x12, 0x0b, 0x0a, 0x07, 0x57, 0x41, 0x52, 0x4e,
	0x49, 0x4e, 0x47, 0x10, 0x02, 0x12, 0x09, 0x0a, 0x05, 0x45, 0x52, 0x52, 0x4f, 0x52, 0x10, 0x03,
	0x22, 0x62, 0x0a, 0x06, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x16, 0x0a, 0x12, 0x53, 0x54,
	0x41, 0x54, 0x55, 0x53, 0x5f, 0x55, 0x4e, 0x53, 0x50, 0x45, 0x43, 0x49, 0x46, 0x49, 0x45, 0x44,
	0x10, 0x00, 0x12, 0x0b, 0x0a, 0x07, 0x55, 0x4e, 0x4b, 0x4e, 0x4f, 0x57, 0x4e, 0x10, 0x01, 0x12,
	0x08, 0x0a, 0x04, 0x4f, 0x50, 0x45, 0x4e, 0x10, 0x02, 0x12, 0x0d, 0x0a, 0x09, 0x43, 0x4f, 0x4e,
	0x46, 0x49, 0x52, 0x4d, 0x45, 0x44, 0x10, 0x03, 0x12, 0x0d, 0x0a, 0x09, 0x43, 0x4f, 0x4d, 0x50,
	0x4c, 0x45, 0x54, 0x45, 0x44, 0x10, 0x04, 0x12, 0x0b, 0x0a, 0x07, 0x45, 0x58, 0x50, 0x49, 0x52,
	0x45, 0x44, 0x10, 0x05, 0x22, 0x27, 0x0a, 0x11, 0x47, 0x65, 0x74, 0x47, 0x61, 0x74, 0x65, 0x77,
	0x61, 0x79, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d,
	0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x22, 0x33, 0x0a,
	0x1b, 0x42, 0x61, 0x74, 0x63, 0x68, 0x52, 0x65, 0x66, 0x72, 0x65, 0x73, 0x68, 0x47, 0x61, 0x74,
	0x65, 0x77, 0x61, 0x79, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x14, 0x0a, 0x05,
	0x6e, 0x61, 0x6d, 0x65, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x09, 0x52, 0x05, 0x6e, 0x61, 0x6d,
	0x65, 0x73, 0x22, 0x57, 0x0a, 0x1c, 0x42, 0x61, 0x74, 0x63, 0x68, 0x52, 0x65, 0x66, 0x72, 0x65,
	0x73, 0x68, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x37, 0x0a, 0x08, 0x67, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x73, 0x18, 0x01,
	0x20, 0x03, 0x28, 0x0b, 0x32, 0x1b, 0x2e, 0x73, 0x69, 0x70, 0x2e, 0x70, 0x61, 0x79, 0x72, 0x65,
	0x78, 0x78, 0x77, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72, 0x2e, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61,
	0x79, 0x52, 0x08, 0x67, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x73, 0x22, 0x58, 0x0a, 0x1d, 0x42,
	0x61, 0x74, 0x63, 0x68, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61,
	0x79, 0x46, 0x65, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x37, 0x0a, 0x08,
	0x67, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x1b,
	0x2e, 0x73, 0x69, 0x70, 0x2e, 0x70, 0x61, 0x79, 0x72, 0x65, 0x78, 0x78, 0x77, 0x72, 0x61, 0x70,
	0x70, 0x65, 0x72, 0x2e, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x52, 0x08, 0x67, 0x61, 0x74,
	0x65, 0x77, 0x61, 0x79, 0x73, 0x22, 0x45, 0x0a, 0x1e, 0x42, 0x61, 0x74, 0x63, 0x68, 0x55, 0x70,
	0x64, 0x61, 0x74, 0x65, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x46, 0x65, 0x65, 0x73, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x23, 0x0a, 0x0d, 0x77, 0x61, 0x72, 0x6e, 0x69,
	0x6e, 0x67, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x09, 0x52, 0x0c,
	0x77, 0x61, 0x72, 0x6e, 0x69, 0x6e, 0x67, 0x4e, 0x61, 0x6d, 0x65, 0x73, 0x32, 0xea, 0x03, 0x0a,
	0x0e, 0x50, 0x61, 0x79, 0x72, 0x65, 0x78, 0x78, 0x77, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72, 0x12,
	0x49, 0x0a, 0x0d, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79,
	0x12, 0x1b, 0x2e, 0x73, 0x69, 0x70, 0x2e, 0x70, 0x61, 0x79, 0x72, 0x65, 0x78, 0x78, 0x77, 0x72,
	0x61, 0x70, 0x70, 0x65, 0x72, 0x2e, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x1a, 0x1b, 0x2e,
	0x73, 0x69, 0x70, 0x2e, 0x70, 0x61, 0x79, 0x72, 0x65, 0x78, 0x78, 0x77, 0x72, 0x61, 0x70, 0x70,
	0x65, 0x72, 0x2e, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x12, 0x50, 0x0a, 0x0a, 0x47, 0x65,
	0x74, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x12, 0x25, 0x2e, 0x73, 0x69, 0x70, 0x2e, 0x70,
	0x61, 0x79, 0x72, 0x65, 0x78, 0x78, 0x77, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72, 0x2e, 0x47, 0x65,
	0x74, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a,
	0x1b, 0x2e, 0x73, 0x69, 0x70, 0x2e, 0x70, 0x61, 0x79, 0x72, 0x65, 0x78, 0x78, 0x77, 0x72, 0x61,
	0x70, 0x70, 0x65, 0x72, 0x2e, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x12, 0x79, 0x0a, 0x14,
	0x42, 0x61, 0x74, 0x63, 0x68, 0x52, 0x65, 0x66, 0x72, 0x65, 0x73, 0x68, 0x47, 0x61, 0x74, 0x65,
	0x77, 0x61, 0x79, 0x73, 0x12, 0x2f, 0x2e, 0x73, 0x69, 0x70, 0x2e, 0x70, 0x61, 0x79, 0x72, 0x65,
	0x78, 0x78, 0x77, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72, 0x2e, 0x42, 0x61, 0x74, 0x63, 0x68, 0x52,
	0x65, 0x66, 0x72, 0x65, 0x73, 0x68, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x73, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x30, 0x2e, 0x73, 0x69, 0x70, 0x2e, 0x70, 0x61, 0x79, 0x72,
	0x65, 0x78, 0x78, 0x77, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72, 0x2e, 0x42, 0x61, 0x74, 0x63, 0x68,
	0x52, 0x65, 0x66, 0x72, 0x65, 0x73, 0x68, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x73, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3f, 0x0a, 0x0d, 0x43, 0x68, 0x65, 0x63, 0x6b,
	0x4c, 0x69, 0x76, 0x65, 0x6e, 0x65, 0x73, 0x73, 0x12, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79,
	0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62,
	0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x12, 0x7f, 0x0a, 0x16, 0x42, 0x61, 0x74, 0x63,
	0x68, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x46, 0x65,
	0x65, 0x73, 0x12, 0x31, 0x2e, 0x73, 0x69, 0x70, 0x2e, 0x70, 0x61, 0x79, 0x72, 0x65, 0x78, 0x78,
	0x77, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72, 0x2e, 0x42, 0x61, 0x74, 0x63, 0x68, 0x55, 0x70, 0x64,
	0x61, 0x74, 0x65, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x46, 0x65, 0x65, 0x73, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x32, 0x2e, 0x73, 0x69, 0x70, 0x2e, 0x70, 0x61, 0x79, 0x72,
	0x65, 0x78, 0x78, 0x77, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72, 0x2e, 0x42, 0x61, 0x74, 0x63, 0x68,
	0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x47, 0x61, 0x74, 0x65, 0x77, 0x61, 0x79, 0x46, 0x65, 0x65,
	0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42, 0x66, 0x0a, 0x15, 0x63, 0x68, 0x2e,
	0x73, 0x69, 0x70, 0x2e, 0x70, 0x61, 0x79, 0x72, 0x65, 0x78, 0x78, 0x77, 0x72, 0x61, 0x70, 0x70,
	0x65, 0x72, 0x50, 0x01, 0x5a, 0x4b, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x65, 0x74, 0x68,
	0x7a, 0x2e, 0x63, 0x68, 0x2f, 0x76, 0x73, 0x65, 0x74, 0x68, 0x2f, 0x30, 0x34, 0x30, 0x33, 0x2d,
	0x69, 0x73, 0x67, 0x2f, 0x6c, 0x69, 0x62, 0x72, 0x61, 0x72, 0x69, 0x65, 0x73, 0x2f, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x73, 0x74, 0x75, 0x62, 0x2d, 0x67, 0x6f, 0x6c, 0x61, 0x6e, 0x67, 0x2f, 0x73,
	0x69, 0x70, 0x2f, 0x70, 0x61, 0x79, 0x72, 0x65, 0x78, 0x78, 0x77, 0x72, 0x61, 0x70, 0x70, 0x65,
	0x72, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescOnce sync.Once
	file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescData = file_sip_payrexxwrapper_payrexxwrapper_proto_rawDesc
)

func file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescGZIP() []byte {
	file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescOnce.Do(func() {
		file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescData = protoimpl.X.CompressGZIP(file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescData)
	})
	return file_sip_payrexxwrapper_payrexxwrapper_proto_rawDescData
}

var file_sip_payrexxwrapper_payrexxwrapper_proto_enumTypes = make([]protoimpl.EnumInfo, 2)
var file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_sip_payrexxwrapper_payrexxwrapper_proto_goTypes = []interface{}{
	(Gateway_State)(0),                     // 0: sip.payrexxwrapper.Gateway.State
	(Gateway_Status)(0),                    // 1: sip.payrexxwrapper.Gateway.Status
	(*Gateway)(nil),                        // 2: sip.payrexxwrapper.Gateway
	(*GetGatewayRequest)(nil),              // 3: sip.payrexxwrapper.GetGatewayRequest
	(*BatchRefreshGatewaysRequest)(nil),    // 4: sip.payrexxwrapper.BatchRefreshGatewaysRequest
	(*BatchRefreshGatewaysResponse)(nil),   // 5: sip.payrexxwrapper.BatchRefreshGatewaysResponse
	(*BatchUpdateGatewayFeesRequest)(nil),  // 6: sip.payrexxwrapper.BatchUpdateGatewayFeesRequest
	(*BatchUpdateGatewayFeesResponse)(nil), // 7: sip.payrexxwrapper.BatchUpdateGatewayFeesResponse
	(*money.Money)(nil),                    // 8: vseth.type.Money
	(*timestamppb.Timestamp)(nil),          // 9: google.protobuf.Timestamp
	(*emptypb.Empty)(nil),                  // 10: google.protobuf.Empty
}
var file_sip_payrexxwrapper_payrexxwrapper_proto_depIdxs = []int32{
	8,  // 0: sip.payrexxwrapper.Gateway.amount:type_name -> vseth.type.Money
	9,  // 1: sip.payrexxwrapper.Gateway.expire_time:type_name -> google.protobuf.Timestamp
	8,  // 2: sip.payrexxwrapper.Gateway.fee:type_name -> vseth.type.Money
	0,  // 3: sip.payrexxwrapper.Gateway.state:type_name -> sip.payrexxwrapper.Gateway.State
	1,  // 4: sip.payrexxwrapper.Gateway.status:type_name -> sip.payrexxwrapper.Gateway.Status
	2,  // 5: sip.payrexxwrapper.BatchRefreshGatewaysResponse.gateways:type_name -> sip.payrexxwrapper.Gateway
	2,  // 6: sip.payrexxwrapper.BatchUpdateGatewayFeesRequest.gateways:type_name -> sip.payrexxwrapper.Gateway
	2,  // 7: sip.payrexxwrapper.Payrexxwrapper.CreateGateway:input_type -> sip.payrexxwrapper.Gateway
	3,  // 8: sip.payrexxwrapper.Payrexxwrapper.GetGateway:input_type -> sip.payrexxwrapper.GetGatewayRequest
	4,  // 9: sip.payrexxwrapper.Payrexxwrapper.BatchRefreshGateways:input_type -> sip.payrexxwrapper.BatchRefreshGatewaysRequest
	10, // 10: sip.payrexxwrapper.Payrexxwrapper.CheckLiveness:input_type -> google.protobuf.Empty
	6,  // 11: sip.payrexxwrapper.Payrexxwrapper.BatchUpdateGatewayFees:input_type -> sip.payrexxwrapper.BatchUpdateGatewayFeesRequest
	2,  // 12: sip.payrexxwrapper.Payrexxwrapper.CreateGateway:output_type -> sip.payrexxwrapper.Gateway
	2,  // 13: sip.payrexxwrapper.Payrexxwrapper.GetGateway:output_type -> sip.payrexxwrapper.Gateway
	5,  // 14: sip.payrexxwrapper.Payrexxwrapper.BatchRefreshGateways:output_type -> sip.payrexxwrapper.BatchRefreshGatewaysResponse
	10, // 15: sip.payrexxwrapper.Payrexxwrapper.CheckLiveness:output_type -> google.protobuf.Empty
	7,  // 16: sip.payrexxwrapper.Payrexxwrapper.BatchUpdateGatewayFees:output_type -> sip.payrexxwrapper.BatchUpdateGatewayFeesResponse
	12, // [12:17] is the sub-list for method output_type
	7,  // [7:12] is the sub-list for method input_type
	7,  // [7:7] is the sub-list for extension type_name
	7,  // [7:7] is the sub-list for extension extendee
	0,  // [0:7] is the sub-list for field type_name
}

func init() { file_sip_payrexxwrapper_payrexxwrapper_proto_init() }
func file_sip_payrexxwrapper_payrexxwrapper_proto_init() {
	if File_sip_payrexxwrapper_payrexxwrapper_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Gateway); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetGatewayRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BatchRefreshGatewaysRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BatchRefreshGatewaysResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BatchUpdateGatewayFeesRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BatchUpdateGatewayFeesResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_sip_payrexxwrapper_payrexxwrapper_proto_rawDesc,
			NumEnums:      2,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_sip_payrexxwrapper_payrexxwrapper_proto_goTypes,
		DependencyIndexes: file_sip_payrexxwrapper_payrexxwrapper_proto_depIdxs,
		EnumInfos:         file_sip_payrexxwrapper_payrexxwrapper_proto_enumTypes,
		MessageInfos:      file_sip_payrexxwrapper_payrexxwrapper_proto_msgTypes,
	}.Build()
	File_sip_payrexxwrapper_payrexxwrapper_proto = out.File
	file_sip_payrexxwrapper_payrexxwrapper_proto_rawDesc = nil
	file_sip_payrexxwrapper_payrexxwrapper_proto_goTypes = nil
	file_sip_payrexxwrapper_payrexxwrapper_proto_depIdxs = nil
}
